import { mapGetters } from 'vuex';

export default {
    name: 'Header',
    computed: {
        ...mapGetters({
            status_: 'search/getStatus',
        }),
    }
};
