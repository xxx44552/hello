module.exports = {
    methods: {
        $formatNumber(n) {
            return new Intl.NumberFormat('uk-UA').format(n);
        }
    }
};
