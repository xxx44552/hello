import { createRouter } from 'vue-router';

const routes = [
    {
        path: '/',
        name: 'Home',
        redirect: '/search', // редірект пустої головної
        component: () => import('../views/Home/index.vue'),
    },
    {
        path: '/:pathMatch(.*)*',
        name: '404',
        component: () => import('../views/404/index.vue'),
    },
];

export default function (history) {
    return createRouter({
        history,
        routes
    });
}
