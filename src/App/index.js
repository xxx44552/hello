import TheHeader from '../components/TheHeader/index.vue';

export default {
    name: 'App',
    components: {
        'the-header': TheHeader,
    },
};
