export default {
    name: 'Filter',
    data() {
        return {
            filters: 'all',
        };
    },
    watch: {
        filters: {
            handler(newValue) {
                this.$emit('setFilters', newValue);
            },
            immediate: true,
        }
    }
};
