import formatNumber from '@mixins/formatNumber.js';
import wordForms from '@mixins/wordForms.js';

export default {
    name: 'Card',
    mixins: [formatNumber, wordForms],
    props: {
        ticket: {
            type: Object,
            default() {
                return {};
            },
        }
    },
    methods: {
        showTime(date, duration) {
            if (!date && !duration) {
                return '';
            }
            const start = new Date(date);
            const config = { hour: '2-digit', minute: '2-digit' };
            return `${start.toLocaleTimeString(['uk-UA'], config)} - ${new Date(start.setMinutes(duration)).toLocaleTimeString(['uk-UA'], config)}`;
        },
        timeConvert(num) {
            const hours = Math.floor(num / 60);
            const minutes = num % 60;
            return `${hours}ч ${minutes}м`;
        },
    }
};
