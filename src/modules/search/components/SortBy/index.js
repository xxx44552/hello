export default {
    name: 'SortBy',
    methods: {
        setSort(type) {
            this.$emit('setSort', type);
        },
    }
};
