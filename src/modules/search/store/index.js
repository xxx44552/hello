export default {
    namespaced: true,
    state: {
        tickets: [],
        status: false,
    },
    mutations: {
        addTickets(state, tickets) {
            state.tickets = state.tickets.concat(tickets);
        },
        setStatus(state, status) {
            state.status = status;
        }
    },
    actions: {
        async fetchSearch({ commit }) {
            const startDate = new Date();
            const host = 'https://front-test.beta.aviasales.ru';
            const idResponse = await fetch(`${host}/search`);
            const { searchId = '' } = await idResponse.json();
            (async function getTickets() {
                const ticketsResponse = await fetch(`${host}/tickets?searchId=${searchId}`);
                if (ticketsResponse.ok) {
                    const { tickets = [], stop } = await ticketsResponse.json();
                    commit('addTickets', tickets);
                    if (!stop) {
                        await getTickets();
                    } else {
                        const endDate = new Date();
                        const seconds = (endDate.getTime() - startDate.getTime()) / 1000;
                        console.log(seconds, '<-- seconds');
                        commit('setStatus', stop);
                    }
                } else {
                    await getTickets();
                }
            }());
        },
    },
    getters: {
        getTickets: ({tickets}) => tickets,
        getStatus: ({status}) => status,
    },
    global: true,
};
