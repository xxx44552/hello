import { mapActions, mapGetters } from 'vuex';

import Filer from '../../components/Filter/index.vue';
import SearchBy from '../../components/SortBy/index.vue';
import Card from '../../components/Card/index.vue';

export default {
    name: 'Search',
    components: {
        'filter-bar': Filer,
        'search-by': SearchBy,
        'card': Card,
    },
    data() {
        return {
            step: 5,
            sort: 'price',
            filterBy: 'all',
            cache: new Map(),
        };
    },
    computed: {
        ...mapGetters({
            tickets_: 'search/getTickets',
            status_: 'search/getStatus',
        }),
        tickets() {
            let _tickets = this.tickets_ || [];
            _tickets = this.cacheFunc(this.filterFunc, _tickets, this.filterBy + this.sort);
            return this.cacheFunc(this.sortFunc, _tickets, this.sort + this.filterBy);
        },
    },
    methods: {
        ...mapActions({
            fetchSearch_: 'search/fetchSearch',
        }),
        setSort(type) {
            this.sort = type;
        },
        setFilters(filter) {
            this.filterBy = filter;
        },
        cacheFunc(f, data, key) {
            if (this.status_ && this.cache.has(key)) {
                return this.cache.get(key);
            }
            const result = f(data);
            if (this.status_) {
                this.cache.set(key, result);
            }
            return result;
        },
        sortFunc(tickets) {
            switch (this.sort) {
            case 'price':
                return tickets.sort((a, b) => a.price - b.price);
            case 'fast':
                return tickets.map(ticket => {
                    const {segments: [ to = {}, from = {} ] = []} = ticket;
                    ticket.speed = to.duration + from.duration;
                    return ticket;
                }).sort((a, b) => a.speed - b.speed);
            case 'optimal':
                return tickets.map(ticket => {
                    const {segments: [ to = {}, from = {} ] = []} = ticket;
                    ticket.optimal = to.stops.length + from.stops.length || 0;
                    return ticket;
                }).sort((a, b) => a.optimal - b.optimal);
            default:
                return tickets;
            }
        },
        filterFunc(tickets) {
            switch (true) {
            case this.filterBy === 'all':
                return tickets;
            case !Number.isNaN(this.filterBy):
                return tickets.filter(ticket => {
                    const {segments: [ to = {}, from = {} ] = []} = ticket;
                    const amount = to.stops.length + from.stops.length;
                    return amount === this.filterBy;
                });
            default:
                return tickets;
            }
        },
    },
    mounted() {
        this.fetchSearch_();
    },
    watch: {
        filterBy() {
            this.step = 5;
        },
        sort() {
            this.step = 5;
        },
    }
};
