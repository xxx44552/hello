const moduleRoute = {
    path: '/Search',
    component: () => import('./Module.vue'),
    children: [
        {
            path: '',
            component: () => import('./views/Search/index.vue')
        },
        // for example
        // {
        //     path: ':cardId',
        //     component: () => import('somePath')
        // }
    ]
};

export default router => {
    router.addRoute(moduleRoute);
};
